# BlackMagic Design DeckLink SDK

This is a bare version of the DeckLink SDK from BlackMagic Design. The original,
fully featured with exampels, can be found at https://www.blackmagicdesign.com/developer/product/capture-and-playback

The C++ Header and Source files are licensed under the [Boost License](https://www.boost.org/users/license.html)
with the copyright owned by BlackMagic Design. This license is an [OSI approved](https://opensource.org/licenses/bsl1.0.html)
MIT-like and compatible with the GPLv2.

This repository also contains the SDK documentation in PDF as well as the ReadMe
that is originally present in official SDK. BlackMagic Design owns the copyright
to these files.

## Usage

To preserve compatibility the files are unmodified and in their original location.

* when targetting Windows, an app should use files from `Win/include`
* when targetting macOS, an app should use files from `Mac/include`
* when targetting Linux, an app should use files from `Linux/include`
